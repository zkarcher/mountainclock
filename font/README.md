# Font Generation Script

### Prerequesites

To run the script for the first time:

1. [Install NodeJS.](https://nodejs.org/en/download/)
2. `npm install`

This will install the required libraries for generating fonts.

Some libraries may require additional steps, including [the canvas library](https://www.npmjs.com/package/canvas).

Windows users will need the Windows Build Tools; one method of installation can be found in the [node-gyp installation instructions](https://github.com/nodejs/node-gyp#installation), option 1.

### Running the script

`node generate_fonts.js`

That's it! The new font data will be embedded in the project when the .ino sketch is compiled.

### Compression format

Font glyphs and images are compressed using a sequence of steps designed to minimize Flash space:

1). Bitmaps are stored as run lengths of "Off" (light) and "On" (dark) pixels. Example:

`[0,0, 1,1,1,1,1] => [2, 5]`

2). The leading light and dark values are stored in the raw, as unsigned integers.

Subsequent values are stored as the difference between the new value and the running total (these are signed integers).

Signed integers are encoded using "zig-zag encoding", which allows us to express values closer to 0 using fewer bits. Example zig-zag value map: (See `font_util.js` for more information.)

```
  0 => 1
  1 => 2
 -1 => 3
  2 => 4
 -2 => 5
  3 => 6
 -3 => 7
   ...
```

Note that no zig-zag value is encoded as 0. Zero is the special REPEAT_BYTE_MARKER, which indicates a repeated sequence should be unrolled:

```
[
   ...

	 REPEAT_BYTE_MARKER,
      repeat_times,
      repeat_byte_count,
      ...(the bytes to repeat)...

   ...(then the data continues as normal)...
]
```

3). Values are converted to bitstreams, and encoded using a technique similar to the [Variable-length Quantity](https://en.wikipedia.org/wiki/Variable-length_quantity) technique.

Traditional VLQ uses a "word length" of 7 bits, preceded by a bit indicating whether the value continues in the next word.

In our compression, each "word" is typically 2 or 3 bits. (The script determines the optimal word size for each font.) Here are two examples, both using word sizes of 2 bits:

`//      1  =>  [0,0,1];          // Leading '0'. The value does not continue: 0b01`

`//      7  =>  [1,0,1,  0,1,1];  // Leading '1', so the value continues in the next word: 0b0111`

In the .ino code, the compression is reversed: The bitstreams are decoded to generate the run lengths.
