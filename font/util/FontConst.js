module.exports = {
	CANVAS_W: 300,
	CANVAS_H: 400,
	DRAW_X: 50,
	DRAW_Y: 350,

	// opentype works OK, doesn't render CJK characters (.otf) correctly
	// fontkit is harder to code against, tho seems to render CJK ok.
	USE_FONTKIT: true,

	DEST_PATH_AR: ["..", "MountainClock"],

	FONTS: [
		{
			"file": "Alvaro-Condensed.otf",
			"cVarName": "FONT_ALVARO_CND",
			"sets": [
				{
					"size": 250,
					"chars": "0123456789",
					"charGroups": ""
				},
				{
					"size": 65,
					"chars": "AMPM",
					"charGroups": ""
				}
			]
		},

		{
			"file": "Logisoso.ttf",
			"cVarName": "FONT_LOGISOSO",
			"sets": [
				{
					"size": 22,
					"chars": "",
					"charGroups": "ascii"
				}
			]
		}
	]
};
