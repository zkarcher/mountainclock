const fs = require('fs');
const path = require('path');

Array.prototype.spliceArray = function(index, n, array) {
	return Array.prototype.splice.apply(this, [index, n].concat(array));
}

function spliceString(str, index, removeCount, replacement) {
	return str.slice(0,index) + (replacement || "") + str.slice(index+removeCount);
}

function leftPad(v, width) {
	if (String(v).length >= width) {
		return String(v);
	}

	return ("            "+v).substr(-width);
}

function saveFile(pathSegmentAr, contents) {
	const outPath = path.join.apply(null, pathSegmentAr);
	console.log("Writing:", outPath);
	fs.writeFileSync(outPath, contents, {encoding:'utf8'});
}

function introspect(obj, indent) {
	if(!obj) return;
	if(typeof obj !== "object") return;

	if(!indent) indent = "";

	_.each(Object.keys(obj), function(key){
		console.log(key, obj[key]);
		introspect(obj[key], indent+"");
	});
}

module.exports = {
	spliceString: spliceString,
	leftPad: leftPad,
	saveFile: saveFile,
	introspect: introspect
};
