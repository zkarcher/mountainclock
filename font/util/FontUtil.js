var path = require('path');
var fontkit = require('fontkit');
var opentype = require('opentype.js');

var FontConst = require('./FontConst.js');

function getFontPath(name) {
	return path.join(__dirname, '..', 'fonts', name);
}

function loadFontFileSync(file) {
	var path = this.getFontPath(file);

	var fontObj;

	if (FontConst.USE_FONTKIT) {
		fontObj = fontkit.openSync(path);
	} else {
		fontObj = opentype.loadSync(path);
	}

	return fontObj;
}

// Encode the values using "zig-zag" encoding: values closer to 0 require fewer bits.
//     (0->1, 1->2, -1->3, 2->4, -2->5, 3->6, -3->7 ...)
//
// Output value 0 is REPEAT_BYTE_MARKER,
// used for encoding repeated number sequences.
//
// Similar algorithms:  https://gist.github.com/mfuerstenau/ba870a29e16536fdbaba
// The least-significant bit contains the sign (0=negative, 1=positive).

function zigZagEncode(n) {
	// Positive
	if (0 < n) {
		return (n << 1);
	}

	// Zero/Negative
	return (Math.abs(n) << 1) + 1;
}

function zigZagDecode(n) {
	// n -> Zero/Negative
	if (n & 0x1) {
		return -(n >> 1);
	}

	// n -> Positive
	return (n >> 1);
}

module.exports = {
	getFontPath: getFontPath,
	loadFontFileSync: loadFontFileSync,
	zigZagEncode: zigZagEncode,
	zigZagDecode: zigZagDecode,
};
