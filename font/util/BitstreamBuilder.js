function BitstreamBuilder() {
	const DEBUG = false;

	this.bytes = [];

	this.workByte = 0;
	this.mask = 0x80;

	this.pushByte = (byte) => {
		if (DEBUG) console.log("BYTE!", byte.toString(2));

		this.bytes.push(byte);
	}

	this.addBit = (b) => {
		if (DEBUG) console.log("bit:", (b ? 1 : 0));

		if (b) {
			this.workByte |= this.mask;
		}

		// Filled this byte?
		this.mask >>= 1;
		if (!this.mask) {
			this.pushByte(this.workByte);
			this.workByte = 0;
			this.mask = 0x80;
		}
	}

	this.appendVLQ = (data, wordSize) => {
		if (typeof data === "number") {
			data = [data];
		}

		const continueSz = (1 << wordSize);
		const wordMask = continueSz - 1;

		for (let d = 0; d < data.length; d++) {
			let value = data[d];
			if (DEBUG) console.log("value:",value);

			// Split the value into word-size pieces
			const words = [];

			do {
				words.unshift(value & wordMask);
				value >>= wordSize;
			} while (value);

			while (words.length) {
				const word = words.shift();

				// Continuation flag
				this.addBit(words.length ? 1 : 0);

				// LSB first, should decode a bit faster?
				let bitMask = (0x1 << (wordSize - 1));
				while (bitMask) {
					this.addBit(word & bitMask);
					bitMask >>= 1;
				}
			}
		}
	}

	this.padCurrentByte = (b) => {
		if (DEBUG) console.log("PAD THIS BYTE:", b);

		while (this.mask !== 0x80) {
			this.addBit(b);
		}
	}
}

module.exports = BitstreamBuilder;
