const { createCanvas } = require('canvas');
var _ = require('lodash');

var FontConst = require('./FontConst.js');
var FontUtil = require('./FontUtil.js');

const DEBUG_REPEATS = false;

const REPEAT_BYTE_MARKER = 0x0;

function drawString(str, font, size){
	var canvas = createCanvas(FontConst.CANVAS_W, FontConst.CANVAS_H);
	var ctx = canvas.getContext('2d');
	ctx.textBaseline="alphabetic";

	// Clear the canvas (fill with solid white)
	ctx.fillStyle = "#ffffff";
	ctx.fillRect(0,0,canvas.width,canvas.height);
	ctx.fillStyle = "#000000";

	// Create font path
	var run;
	if (FontConst.USE_FONTKIT) {
		var baseSize = font.unitsPerEm;	// 2048

		// Only 1 glyph at a time ? :\
		run = font.layout(str);

		var atX = FontConst.DRAW_X;
		for( var i=0; i<run.glyphs.length; i++ ) {
			//console.log(run.positions[i]);
			//console.log(run.glyphs[i].bbox);

			ctx.setTransform( 1,0,0,-1, atX, FontConst.DRAW_Y );
			run.glyphs[i].render(ctx, size);

			atX += (size/baseSize) * run.positions[i].xAdvance;
		}

	} else {
		var font_p = FontUtil.get_path(str, FontConst.DRAW_X, FontConst.DRAW_Y, size, {kerning:true});
		font_p.draw(ctx);
	}

	var imgData = ctx.getImageData(0, 0, FontConst.CANVAS_W, FontConst.CANVAS_H);

	return {canvas:canvas, ctx:ctx, imgData:imgData, run:run};
}

function isPxSet( imgData, i, j ) {
	var alpha = imgData.data[(j*imgData.width+i)*4 + 3];	// RRGGBBAA
	if (alpha < 128) return false;

	var red = imgData.data[(j*imgData.width+i)*4];	// RRGGBBAA
	return (red < 128);	// darker than 50% is considered set
}

// Returns: {.x, .y, .width, .height}
function getDrawingBounds(imgData){
	let left = Infinity;
	let right = -Infinity;
	let top = Infinity;
	let bottom = -Infinity;

	// It's faster if we don't scan the entire canvas.
	const xMin = 0;
	const xMax = FontConst.CANVAS_W;
	const yMin = 0;
	const yMax = FontConst.CANVAS_H;

	for (let i = xMin; i < xMax; i++) {
		for (let j = yMin; j < yMax; j++) {
			let isSet = isPxSet(imgData, i, j);
			if (isSet) {
				left = Math.min(left, i);
				right = Math.max(right, i);
				top = Math.min(top, j);
				bottom = Math.max(bottom, j);
			}
		}
	}

	// Drew nothing? (Space bar?)
	if (right < left) {
		return {x: 0, y: 0, width: 0, height: 0};
	}

	return {
		x: left,
		y: top,
		width: (right - left) + 1,
		height: (bottom - top) + 1
	};
}

// Returns array of bits (boolean)
function getGlyphBits(imgData, bounds, isAcross) {
	const out = [];

	if (isAcross) {
		for (let j = 0; j < bounds.height; j++) {
			for (let i = 0; i < bounds.width; i++) {
				out.push(isPxSet(imgData, bounds.x + i, bounds.y + j));
			}
		}

	} else {
		for (let i = 0; i < bounds.width; i++) {
			for (let j = 0; j < bounds.height; j++) {
				out.push(isPxSet(imgData, bounds.x + i, bounds.y + j));
			}
		}
	}

	return out;
}

// Run lengths:
// [0,0, 1,1,1,1,1]  =>  [2, 5]
function bitArrayToRunLengths(bits) {
	bits = bits.slice(0);	// clone array

	var lens = [];

	// Remove empty (off) bits from the end. We don't need to
	while( bits.length ) {
		if( bits[bits.length-1] ) break;
		bits.pop();
	}

	while( bits.length ) {
		// Get the number of upcoming off & on bits.
		var offCount = 0;
		for( var i = 0; i<bits.length; i++ ) {
			if( bits[i] ) break;	// Found an 'on' bit
			offCount++;
		}

		var onCount = 0;
		for( var i = offCount; i<bits.length; i++ ) {
			if( !bits[i] ) break;	// Found an 'off' bit
			onCount++;
		}

		// Store off & on run lengths
		lens.push(offCount, onCount);

		// Splice these bits off the array, we have encoded them.
		bits.splice( 0, offCount + onCount );
	}

	return lens;
}

function runLengthsToDataValues(lens) {
	var out = [];

	// The first two values are absolute (positive numbers), not zig-zag encoded.
	// Subsequent numbers are stored as the delta (change) compared to the previous value.
	var prevOff = -1;
	var prevOn = -1;

	// Run lengths always come in pairs: Off (light), then On (dark)
	for (let i = 0; i < lens.length; i += 2) {
		if (i === 0) {
			out.push( lens[0], lens[1] );

		} else {
			dOff = FontUtil.zigZagEncode(lens[i] - prevOff);
			dOn = FontUtil.zigZagEncode(lens[i + 1] - prevOn);
			out.push(dOff, dOn);
		}

		prevOff = lens[i];
		prevOn = lens[i + 1];
	}

	const MAX_REP_LENGTH = 6;

	// Try to find a repeating sequence that
	// we can reduce to a smaller expression.
	// Skip the initial on/off values.
	for (let i = 2; i < out.length; i++) {

		// How many repeats of size r at position i:
		const repCount = [0];

		for (let r = 1; r <= MAX_REP_LENGTH; r++) {
			repCount[r] = 0;

			let readOffset = 0;

			for (let compare = i + r; compare < out.length; compare++) {
				if (out[i + readOffset] !== out[compare]) {
					break;
				}

				// Complete copy of data[i:r]?
				readOffset++;
				if (r <= readOffset) {
					repCount[r]++;
					readOffset = 0;
				}
			}
		}

		// Which repcount is best?
		let bestR = 0;
		for (let r = 1; r <= MAX_REP_LENGTH; r++) {
			// Loop/repeat instruction uses 3 values.
			// If we can't compress >4 values,
			// it's probably not worth doing.
			const saveValues = repCount[r] * r;
			if (saveValues < 4) {
				continue;
			}

			if (repCount[bestR] * bestR < saveValues) {
				bestR = r;
			}
		}

		// Do we benefit from a loop/repeat here?
		if (bestR) {
			out.splice(
				i,	// splice index
				bestR * repCount[bestR],	// remove this many values
				0,	// REPEAT_BYTE_MARKER
				repCount[bestR],	// loop count
				bestR	// number of values to repeat
			);

			i += 3 + bestR;
		}
	}

	return out;
}

// ZKA: This Huffman encoding table results in _longer_ data, so I'm not using it:
//                  1 => [1]
//                  0 => [0, 0]
//   Value terminator => [0, 1]

/*
function runLengthsToHuffmanEncodedBitstream(lens) {
	var out = [];

	while (lens.length) {
		var value = lens.shift();

		// Split value into an array of binary bits: 0x1011 -> [1,0,1,1]
		var binary = [];
		while (value) {
			binary.unshift(value & 0x1);
			value >>= 1;
		}

		_.each(binary, function(b){
			if (b) {
				out.push(1);
			} else {
				out = out.concat([0,0]);
			}
		});

		// Add the value terminator
		out = out.concat([0,1]);
	}

	return out;
}
*/

// Returns:
// {
//    "word_size": number,
//    "byte_count": number
// }

function getBestWordSize(data_values) {
	// We now have all the data values.
	// Using these numbers: Determine the best word size (in bits) to use for
	// the compression of this font.
	let bestByteCount = Infinity;
	let bestBitCount = Infinity;
	let bestWordSize = 0;

	// Maximum bits-per-word. This is somewhat arbitrary.
	// So far, the optimal word length for font data is: 2 for most fonts, 3 for
	// some larger fonts. 7 is probably overkill, but check it, anyways.
	const MAX_WORD_LENGTH = 7;

	let word_size_byte_counts = [0];

	for (let sz = 1; sz <= MAX_WORD_LENGTH; sz++) {

		// How many words to store this data?
		let wc = 0;

		for (let i = 0; i < data_values.length; i++) {
			// Zero: Still needs to be stored as a word. For evaluating size: treat this as 1.
			let value = (data_values[i] || 1);

			while (value > 0) {
				wc++;
				value >>= sz;
			}
		}

		// Round up to the next whole byte.
		// Remember to include 1 continue bit per word,
		// indicating whether value continues in next word.
		let bit_count = wc * (sz + 1);
		let byte_count = Math.ceil(bit_count / 8.0);

		word_size_byte_counts[sz] = byte_count;

		//if (byte_count < bestByteCount) {
		if (bit_count < bestBitCount) {
			bestByteCount = byte_count;
			bestBitCount = bit_count;
			bestWordSize = sz;
		}
	}

	/*
	console.log("\t", _.map(word_size_byte_counts, function(v, i){
		return "[" + i + "]:" + v;
	}).join(", "));
	console.log("\t", "Best word size:", bestWordSize, "bestByteCount:", bestByteCount, "bestBitCount:", bestBitCount);
	*/

	return {
		"word_size": bestWordSize,
		"byte_count": bestByteCount
	};
}

// Encode data values (run lengths) as an array of bits.
// Example: With word_size == 2:
// (Each word has a leading it, indicating whether value continues in the next word.)
//      1  =>  [0,0,1];          // Leading '0', the value does not continue.
//      7  =>  [1,0,1,  0,1,1];  // Leading '1', the value continues in the next word.

function data_values_to_bit_stream(value_ar, word_size) {
	const DEBUG_BIT_STREAM = false;

	var bits = [];

	var maxWordValue = (1 << word_size) - 1;

	if (DEBUG_BIT_STREAM) console.log(" -------------------------- ");

	for (var i = 0; i < value_ar.length; i++) {
		var value = value_ar[i];
		var bits_length_at_start = bits.length;

		// Split the value into word-size pieces
		var pieces = [];
		while (true) {
			pieces.unshift(value & maxWordValue);
			value >>= word_size;
			if (value == 0) break;
		}

		// Encode each word, add to the bitstream. If the next word should be appended to
		// this one (i.e. the value is larger than maxWordValue): Prepend bit '1', otherwise '0'.
		while (pieces.length) {
			var word = pieces.shift();

			// If the value continues (with more words): Prepend bit '1', otherwise '0'.
			bits.push( pieces.length ? 1 : 0 );

			// Store each bit
			for (var b = word_size - 1; b >= 0; b--) {
				bits.push( (word & (1 << b)) ? 1 : 0 );
			}
		}

		if (DEBUG_BIT_STREAM) console.log(value_ar[i], bits.slice(bits_length_at_start));
	}

	// Artifacts at the end: Just append a bunch of '11111's to the bits, to fill the
	// final byte. This will build an ever-larger numeric value without drawing anything.
	while ((bits.length % 8) != 0) {
		bits.push(1);
	}

	return bits;
}

/*
function bit_stream_to_bytes(bits) {
	var out = [];

	var byte = 0;
	var mask = (1 << 7);
	for (var i = 0; i < bits.length; i++) {
		if (bits[i]) {
			byte |= mask;
		}

		mask >>= 1;
		if (mask == 0) {
			// Reset the byte
			out.push(byte);
			byte = 0;

			// Reset the mask
			mask = (1 << 7);
		}
	}

	// Bit iteration is finished. Any data in this byte? Store it.
	if (mask != (1 << 7)) {
		out.push(byte);
	}

	return out;
}
*/

// Run-length encoding. Typical byte format:
//        CWWWWBBB (C: Values continue at next byte,
//                  4 bits of white (off), 3 bits of black (on)).
//
// Compress repeated/duplicated sequences:
//      Replace any basic run lengths 255 with 2 bytes. And then:
//      255, [3b bytes to repeat][5b repeat number of times] b0 b1 b2 ...

/*
// 2017-03-20 ZKA: Deprecated, not used anymore. We have better compression now.
//                 (See: data_values_to_bit_stream)
//                 There is code in this function for finding repeated sequences;
//                 this might be useful later.
function compress_bits( bits ) {
	var bytes = [];

	// Remove empty (off) bits from the end. We don't need to
	while( bits.length ) {
		if( bits[bits.length-1] ) break;
		bits.pop();
	}

	// Run lengths are stored as deltas (change compared to previous run length).
	// Hopefully this will result in smaller byte counts.
	var prevOff = -1;
	var prevOn = -1;

	while( bits.length ) {
		// Get the number of upcoming off & on bits.
		var offCount = 0;
		for( var i=0; i<bits.length; i++ ) {
			if( bits[i] ) break;	// Found an 'on' bit
			offCount++;
		}

		var onCount = 0;
		for( var i=offCount; i<bits.length; i++ ) {
			if( !bits[i] ) break;	// Found an 'off' bit
			onCount++;
		}

		// Build an array of bytes to append, right-to-left.
		// (Extract the least-significant info first.)
		append = [];
		var offTemp = offCount - prevOff;
		var onTemp = onCount - prevOn;

		// Possible savings?: Only use zig-zag encoding for values _after_
		// the first ones.
		if (prevOff != -1) {
			offTemp = zigZagEncode(offTemp);
			onTemp = zigZagEncode(onTemp);
		}

		while ((offTemp > 0) || (onTemp > 0)) {
			var b = ((offTemp & 0b1111) << 3) | (onTemp & 0b111);
			append.push(b);
			offTemp >>= 4;
			onTemp >>= 3;
		}

		// Sequences of more than 1 byte (values larger than 16/8):
		// Set the MSB to 1, indicating the bytes should be compounded.
		for (var i = 0; i < append.length-1; i++) {
			append[i] |= 0x80;
		}

		bytes = bytes.concat(append);

		// Splice these bits off the array, we have encoded them.
		bits.splice( 0, offCount + onCount );

		// Store off & on counts, we are storing the changes (not absolute values).
		prevOff = offCount;
		prevOn = onCount;
	}

	// Look for repeated sequences. Try to find longer repeats first,
	// fall back on short repeats.
	i = 0;
	while (i < bytes.length) {
		repeatCounts = [0]

		for (var len = 1; len <= 8; len++) {
			var repeatCount = 1;	// Default is 1: 1 instance of the bytes.

			while (repeatCount < 32) {
				var is_repeating_here = true;

				// Check each byte, ensure that repetition is happening
				for (var b = 0; b < len; b++) {
					if (bytes[i + b] != bytes[i + len * repeatCount + b]) {
						is_repeating_here = false;
						break;
					}
				}

				if (!is_repeating_here) break;

				repeatCount++;
			}

			repeatCounts[len] = repeatCount;
		}

		// We know how many times this sequence repeats, at the different lengths.
		// Choose the smallest option (or do not repeat, if no savings is possible).
		var bestLen = 0;
		var bestByteSavings = 0;
		var bestRepeatCount = 0;
		for (var len = 1; len <= 8; len++) {
			var byteCountNow = len * repeatCounts[len];
			// Repeat header is: REPEAT_BYTE_MARKER, then a descriptor byte
			var byteCountAfterRepeat = 2 + len;

			var savings = (byteCountAfterRepeat - byteCountNow);
			if (savings < bestByteSavings) {
				//console.log("better because now:", byteCountNow, "after:", byteCountAfterRepeat, "so the savings is:", savings);
				bestLen = len;
				bestByteSavings = savings;
				bestRepeatCount = repeatCounts[len];
			}
		}

		if (bestLen != 0) {
			var len = bestLen;
			var repeatCount = bestRepeatCount;

			if (DEBUG_REPEATS) console.log("applying repeat: i:", i, "len(bytes):", bytes.length, "count:", repeatCount, "len:", len, bytes.slice(i, i + len));

			// Splice out the redundant bytes
			bytes.splice(i, len * (repeatCount-1));

			// Insert the 0 marker, and the repeat descriptor
			var descriptorByte = ((len-1) << 5) | (repeatCount-1);
			bytes.splice(i, 0, REPEAT_BYTE_MARKER, descriptorByte);

			// Advance past the REPEAT_BYTE_MARKER, descriptor, and len.
			i += 2 + len;
			continue;
		}

		// Unable to achieve repeat-compression savings at this position.
		// Try the next byte.
		i++;

	}	// end while(i < bytes.length)

	return bytes;
}
*/

function save_canvas_png( canvas, outName ){
	var outFile = fs.createWriteStream(__dirname + '/' + outName);
	var stream = canvas.pngStream();

	stream.on('data', function(chunk){
	  outFile.write(chunk);
	});

	stream.on('end', function(){
	  console.log('Saved:', outName);
	});
}

module.exports = {
	drawString: drawString,
	isPxSet: isPxSet,
	getDrawingBounds: getDrawingBounds,
	getGlyphBits: getGlyphBits,
	bitArrayToRunLengths: bitArrayToRunLengths,
	runLengthsToDataValues: runLengthsToDataValues,
	getBestWordSize: getBestWordSize,
	/*
	//runLengthsToHuffmanEncodedBitstream: runLengthsToHuffmanEncodedBitstream,
	data_values_to_bit_stream: data_values_to_bit_stream,
	*/
};
