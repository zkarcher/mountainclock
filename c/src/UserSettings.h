#pragma once

#include <stdbool.h>
#include <stdint.h>
#include <Preferences.h>

class UserSettings {

public:

	UserSettings();
	void init(Preferences * _prefs);

	uint8_t getWakeHour();
	uint8_t getWakeMinute();
	int32_t getWakeTimeInSeconds();
	uint8_t getLedCount();
	uint32_t getFadeInSeconds();
	uint32_t getWakeDurationSeconds();

	// Returns true if successful, false if
	// arguments are invalid.
	bool setWakeTime(uint8_t hour, uint8_t minute);
	bool setLedCount(uint8_t ledCount);
	bool setFadeInSeconds(int32_t seconds);
	bool setWakeDurationSeconds(int32_t seconds);

protected:

	// Defaults
	Preferences * prefs = NULL;
	uint8_t wakeHour = 12;
	uint8_t wakeMinute = 0;
	uint8_t ledCount = 12;
	int32_t fadeInSeconds = 15 * 60;
	int32_t wakeDurationSeconds = 60 * 60;

};
