/*
 * @author zkarcher
 */

#pragma once

#include <stdint.h>

typedef struct vpoint {
	int16_t x;
	int16_t y;
	const uint8_t * pattern8;
	const uint16_t * pattern16;
	bool flipX;
} VPoint;

typedef enum {
	k_layout_random = 0,
	k_layout_diagonal_line,
	k_layout_ring,
	k_layout_sediment,
	k_layout_grid,
	k_layout_spiral,
	k_layout_MAX
} VLayout;

class VoronoiDiagram {

public:

	VoronoiDiagram();

	// Choose a different layout semi-randomly
	void newLayout(VPoint * vpts, uint8_t vptCount, int16_t width, int16_t height);

	void createLayout(VPoint * vpts, uint8_t vptCount, int16_t width, int16_t height, VLayout layout);

	void scoochLayout(VPoint * vpts, uint8_t vptCount);

protected:

	VLayout lastLayout = k_layout_MAX;

};
