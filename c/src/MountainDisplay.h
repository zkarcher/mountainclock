#pragma once

#include <stdint.h>
#include <Arduino.h>
#include <GxEPD2_BW.h>
#include <WiFi.h>
#include "DateTimeController.h"
#include "VoronoiDiagram.h"

// EPD pins
#define EPD_CS   (SS)
#define EPD_DC   (17)
#define EPD_RST  (16)
#define EPD_BUSY (4)

#define DEV_REDRAW_EVERY_FRAME    (true)
#define VORONOI_PT_COUNT          (30)

#define MESSAGE_LEN               (50)
#define MESSAGE_HEIGHT            (31)	// 8*n - 1 plz
#define MESSAGE_VIEWS             (3)

const int8_t SMALL_TEXT_OUTLINE = 2;

class MountainDisplay {

public:

	MountainDisplay();
	void init(GxEPD2_BW<GxEPD2_420, GxEPD2_420::HEIGHT> * _display);

	void update(const DateTime * dt, const IPAddress * address);

	void showMessage(const char * msg, const bool drawNow);

protected:

	// Tried using GxEPD2_GFX base class ... Plain-and-simple does not work,
	// even with the #define flag
	GxEPD2_BW<GxEPD2_420, GxEPD2_420::HEIGHT> * display;

	//void drawIPAddress(const IPAddress * address);

	VPoint * getClosestVoronoiPoint(int16_t x, int16_t y);
	void drawMessage();

	uint16_t bg;
	uint16_t fg;
	uint8_t patternPhaseX;
	uint8_t patternPhaseY;
	bool hasEverDrawn = false;

	VPoint vpts[VORONOI_PT_COUNT];
	VoronoiDiagram vdiagram;

	// For setting border color, maybe more:
	//EPDExt * epdExt;

	char message[MESSAGE_LEN + 1];
	int32_t messageViews = MESSAGE_VIEWS + 1;
};
