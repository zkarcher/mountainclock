#include <stdint.h>
#include "VLQDecoder.h"

#define REPEAT_BYTE_MARKER     (0)

VLQDecoder::VLQDecoder(const uint8_t * inStream, const uint8_t * inStreamEnd, uint8_t inWordSize)
{
	setStream(inStream, inStreamEnd, inWordSize);
}

const uint8_t * VLQDecoder::getStream()
{
	return stream;
}

void VLQDecoder::setStream(const uint8_t * inStream, const uint8_t * inStreamEnd, uint8_t inWordSize)
{
	stream = inStream;
	streamEnd = inStreamEnd;
	wordSize = inWordSize;
	bitMask = 0x80;
	repeatLoopsRemain = 0;
}

const uint8_t * VLQDecoder::getStreamEnd()
{
	return streamEnd;
}

uint8_t VLQDecoder::getBitMask()
{
	return bitMask;
}

uint8_t VLQDecoder::getWordSize()
{
	return wordSize;
}

void VLQDecoder::copy(VLQDecoder * aDecoder)
{
	stream = aDecoder->getStream();
	streamEnd = aDecoder->getStreamEnd();
	wordSize = aDecoder->getWordSize();
	bitMask = aDecoder->getBitMask();
}

void VLQDecoder::setWordSize(uint8_t inWordSize)
{
	wordSize = inWordSize;
}

// Returns 0 or 1.
inline uint8_t VLQDecoder::getBit()
{
	uint8_t result = (*stream) & bitMask;

	bitMask >>= 1;

	// Advance to next byte?
	if (!bitMask) {
		stream++;
		bitMask = 0x80;
	}

	return (result ? 1 : 0);
}

uint32_t VLQDecoder::decodeUint32()
{
	uint32_t out = 0;

	uint8_t doesWordContinue = 0;

	do {
		if (isDecodeFinished()) {
			return 0;
		}

		doesWordContinue = getBit();

		for (uint8_t w = wordSize; w; w--) {
			out = (out << 1) | getBit();
		}

	} while (doesWordContinue);

	return out;
}

// Decode long runs of signed ints,
// support REPEAT_BYTE_MARKER automagically.
int32_t VLQDecoder::decodeZigZagInt32()
{
	if (isDecodeFinished()) {
		return 0;
	}

	uint32_t uout = decodeUint32();

	// Special flags
	if (uout == REPEAT_BYTE_MARKER) {
		// Start repeated sequence.
		repeatLoopsRemain = decodeUint32();
		repeatWordCount = decodeUint32();
		repeatStart = stream;
		repeatBitMask = bitMask;
		repeatWordsRemain = repeatWordCount;

		// Repeat is set up. Read another uout,
		// process like normal.
		return decodeZigZagInt32();
	}

	// Trigger repeat loops
	if (repeatLoopsRemain) {
		repeatWordsRemain--;

		// Finished this loop?
		if (!repeatWordsRemain) {
			repeatLoopsRemain--;	// This loop is completed

			// Reset words-to-read
			repeatWordsRemain = repeatWordCount;

			// Stream position jumps back
			stream = repeatStart;
			bitMask = repeatBitMask;
		}
	}

	// Negative/zero
	if (uout & 0x1) {
		return (int32_t)(-(uout >> 1));
	}

	// Positive
	return (int32_t)(uout >> 1);
}

bool VLQDecoder::isDecodeFinished()
{
	return (streamEnd <= stream);
}
