#pragma once

#include "DateTimeController.h"
#include "MountainDisplay.h"
#include "UserSettings.h"

void http_server_setup(DateTimeController * dtc, MountainDisplay * display, UserSettings * settings);
void http_server_handle();
