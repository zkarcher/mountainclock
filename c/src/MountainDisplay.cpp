#include <stdint.h>
#include <string.h>
#include <Arduino.h>
#include <GxEPD2_BW.h>

//#include "EPDExt.h"
#include "BitmapRenderer.h"
#include "MountainDisplay.h"
#include "HalftonePatterns.h"
#include "VoronoiDiagram.h"
#include "font_alvaro_cnd.h"
#include "font_logisoso.h"

const uint8_t * BIG_FONT = &FONT_ALVARO_CND_250[0];
const uint8_t * AMPM_FONT = &FONT_ALVARO_CND_65[0];
const uint8_t * SMALL_FONT = &FONT_LOGISOSO_22[0];

// Class template. When I instantiate this as a class,
// the image is flipped horizontally (maybe the EPD init's
// with default values?)
// 2023-11-22: Trying to init in main.cpp instead, pass the pointer
// to the MountainDisplay instance.
//GxEPD2_BW<GxEPD2_420, GxEPD2_420::HEIGHT> display(GxEPD2_420(EPD_CS, EPD_DC, EPD_RST, EPD_BUSY));

MountainDisplay::MountainDisplay()
{
	//epdExt = new EPDExt(EPD_CS, EPD_DC, EPD_RST, EPD_BUSY);
}

void MountainDisplay::init(GxEPD2_BW<GxEPD2_420, GxEPD2_420::HEIGHT> * _display)
{
	message[0] = '\0';

	display = _display;
	display->init();
}

void MountainDisplay::update(const DateTime * dt, const IPAddress * address)
{
	// Messages can only be viewed ~3 times.
	messageViews++;

	display->setRotation(3);

	bool fullScreenUpdate = (!hasEverDrawn) || ((dt->minute % 5) == 0);

	if (DEV_REDRAW_EVERY_FRAME) {
		fullScreenUpdate = true;
	}

	int16_t width = display->width();
	int16_t height = display->height();

	if (fullScreenUpdate) {
		display->setFullWindow();
	} else {
		display->setPartialWindow(0, 0, width, height);
	}

	if (fullScreenUpdate) {
		// Reroll the colors
		if (random(2)) {
			bg = GxEPD_BLACK;
			fg = GxEPD_WHITE;
		} else {
			bg = GxEPD_WHITE;
			fg = GxEPD_BLACK;
		}
	}

	// On partial update: Background color should stay
	// the same. Use a floating voltage.
	//bool isFloating = !fullScreenUpdate;
	//epdExt->setBorderColor(bg, isFloating);

	if (fullScreenUpdate) {
		// Choose a new halftone
		const Halftone * halftone = &HALFTONES[random(HALFTONE_COUNT)];

		uint8_t pattCount = halftone->patternCounts[random(2)];

		bool flipX = (random(2) ? true : false);

		// Re-roll the Voronoi
		vdiagram.newLayout(&vpts[0], VORONOI_PT_COUNT, width, height);

		// Apply patterns and flips
		for (uint8_t v = 0; v < VORONOI_PT_COUNT; v++) {

			vpts[v].flipX = flipX;

			if (halftone->data8 != NULL) {
				vpts[v].pattern8 = &halftone->data8[random(pattCount) * 8];
				vpts[v].pattern16 = NULL;

			} else if (halftone->data16 != NULL) {
				vpts[v].pattern8 = NULL;
				vpts[v].pattern16 = &halftone->data16[random(pattCount) * 16];
			}

		}

		patternPhaseX = random(0xff);
		patternPhaseY = random(0xff);

	} else {
		vdiagram.scoochLayout(&vpts[0], VORONOI_PT_COUNT);
	}

	// The image is redrawn multiple times, filling different
	// regions of the display. Because RAM is scarce.
	//
	// Explanation of paging system:
	//  https://github.com/olikraus/u8glib/wiki/tpictureloop

	display->firstPage();
	do {

		// Voronoi halftone: Draw each pixel
		for (int16_t y = 0; y < height; y++) {
			for (int16_t x = 0; x < width; x++) {

				// Voronoi polygons are convex. For speed,
				// find where the blob ends, then paint the row.
				// Old, non-optimized version: ~2900ms per redraw
				// New, optimized version:     ~ 130ms per redraw

				VPoint * bestPt = getClosestVoronoiPoint(x, y);

				// Binary search for the right-most pixel in this blob:
				int16_t xLeft = x;
				int16_t xRight = width;	// <- Binary search result

				while (true) {
					int16_t xMiddle = (xLeft + xRight) / 2;

					VPoint * newPt = getClosestVoronoiPoint(xMiddle, y);

					// If our search range is <=1: we're basically done.
					if ((xRight - xLeft) <= 1) {
						if (newPt == bestPt) {
							xRight = xMiddle;
							break;

						} else {
							xRight = xLeft;
							break;
						}
					}

					// Search continues, with more granularity:
					if (newPt == bestPt) {
						xLeft = xMiddle;	// Extend right
					} else {
						xRight = xMiddle;	// Shrink left
					}
				}

				// Prepare halftone pattern for this row
				bool isPattern16 = (bestPt->pattern16 != NULL);
				uint8_t pattMask = (isPattern16 ? 0xf : 0x7);	// 8 or 16 lines
				uint8_t pattV = ((patternPhaseY + y) & pattMask);

				uint16_t tx;

				if (isPattern16) {
					tx = bestPt->pattern16[pattV];
				} else {
					// Repeat 8 bits to create 16-bit pattern
					tx = bestPt->pattern8[pattV];
					tx |= (tx << 8);
				}

				// Shift texture for pattU
				uint8_t pattU = (patternPhaseX & 0xf);
				tx = (tx << pattU) | (tx >> (16 - pattU));

				// Flip horizontal?
				if (bestPt->flipX) {
					tx = ((tx & 0xff00) >> 8) | ((tx & 0x00ff) << 8);
					tx = ((tx & 0xf0f0) >> 4) | ((tx & 0x0f0f) << 4);
					tx = ((tx & 0xcccc) >> 2) | ((tx & 0x3333) << 2);
					tx = ((tx & 0xaaaa) >> 1) | ((tx & 0x5555) << 1);
				}

				// Paint pixels, x thru xRight
				uint16_t txMask = (0x01 << (x & 0xf));
				for (int16_t px = x; px <= xRight; px++) {
					display->drawPixel(px, y, (tx & txMask) ? fg : bg);

					// Advance txMask
					txMask <<= 1;
					if (!txMask) txMask = 0x1;
				}

				// x thru xRight is painted, skip over this region:
				x = xRight;
			}
		}

		// Fancy lettering
		BitmapRenderer rndrr(display);
		char buf[50];

		const int16_t ALVARO_LETTER_SPACING = -8;
		const int16_t DRAW_X = 143;
		const int16_t RIGHT_COL_X = DRAW_X + 26;
		const int16_t RIGHT_COL_Y = 218;
		const int16_t RIGHT_COL_SPACING = 28;

		// Hours
		sprintf(&buf[0], "%i", dt->hour12);
		rndrr.drawTextOutline(
			BIG_FONT,
			buf,
			DRAW_X,
			11,
			k_align_right | k_align_hanging,
			ALVARO_LETTER_SPACING,
			fg,
			bg
		);

		// Minutes
		if (dt->minute < 10) {
			sprintf(&buf[0], "0%i", dt->minute);
		} else {
			sprintf(&buf[0], "%i", dt->minute);
		}

		rndrr.drawTextOutline(
			BIG_FONT,
			buf,
			DRAW_X,
			215,
			k_align_right | k_align_hanging,
			ALVARO_LETTER_SPACING,
			fg,
			bg
		);

		const char * MONTHS[] = {
			"INVALID", "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"
		};

		const char * WEEKDAYS[] = {
			"INVALID", "Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"
		};

		// Weekday
		rndrr.drawTextOutline(
			SMALL_FONT,
			WEEKDAYS[dt->weekday],
			RIGHT_COL_X,
			RIGHT_COL_Y,
			k_align_left | k_align_hanging,
			0,
			fg,
			bg,
			SMALL_TEXT_OUTLINE
		);

		// Month & day-of-month
		sprintf(buf, "%s %i", MONTHS[dt->month], dt->day);

		rndrr.drawTextOutline(
			SMALL_FONT,
			buf,
			RIGHT_COL_X,
			RIGHT_COL_Y + RIGHT_COL_SPACING,
			k_align_left | k_align_hanging,
			0,
			fg,
			bg,
			SMALL_TEXT_OUTLINE
		);

		// Year
		sprintf(buf, "%i", dt->year);
		rndrr.drawTextOutline(
			SMALL_FONT,
			buf,
			RIGHT_COL_X,
			RIGHT_COL_Y + RIGHT_COL_SPACING * 2,
			k_align_left | k_align_hanging,
			0,
			fg,
			bg,
			SMALL_TEXT_OUTLINE
		);

		// AM / PM
		rndrr.drawTextOutline(
			AMPM_FONT,
			(dt->isAM ? "AM" : "PM"),
			RIGHT_COL_X,
			344,
			k_align_left | k_align_hanging,
			ALVARO_LETTER_SPACING / 5,
			fg,
			bg
		);

		if (messageViews <= MESSAGE_VIEWS) {
			drawMessage();
		}

	}
	while(display->nextPage());

	hasEverDrawn = true;
}

void MountainDisplay::showMessage(const char * msg, bool drawNow)
{
	int16_t width = display->width();

	strncpy(message, msg, MESSAGE_LEN);
	message[strlen(msg)] = '\0';
	message[MESSAGE_LEN] = '\0';	// Safety string terminator

	if (drawNow) {
		// Partial update: Draw the message now.
		display->setPartialWindow(0, 0, width, MESSAGE_HEIGHT + 1);

		do {
			drawMessage();
		}
		while(display->nextPage());

		messageViews = 1;

	} else {
		messageViews = 0;
	}
}

// -- PROTECTED --

VPoint * MountainDisplay::getClosestVoronoiPoint(int16_t x, int16_t y)
{
	VPoint * bestPt = &vpts[0];
	int32_t bestDistance = INT32_MAX;

	// Find the closest VPoint
	for (uint8_t v = 0; v < VORONOI_PT_COUNT; v++) {
		int16_t dx = x - vpts[v].x;
		/*
		if (bestDistance < abs(dx)) {
			continue;
		}
		*/

		int16_t dy = y - vpts[v].y;
		/*
		if (bestDistance < abs(dy)) {
			continue;
		}
		*/

		// No need for square root, too expensive.
		int32_t dist = dx * dx + dy * dy;

		if (dist < bestDistance) {
			bestDistance = dist;
			bestPt = &vpts[v];
		}
	}

	return bestPt;
}

void MountainDisplay::drawMessage()
{
	int16_t width = display->width();

	display->fillRect(0, 0, width, MESSAGE_HEIGHT, bg);
	display->drawFastHLine(0, MESSAGE_HEIGHT, width, fg);

	// Print the message
	const int16_t AT_X = 8;
	const int16_t AT_Y = 8;

	BitmapRenderer rndrr(display);
	rndrr.drawText(
		SMALL_FONT,
		message,
		AT_X,
		AT_Y,
		k_align_left | k_align_hanging,
		0,
		fg
	);

}
