#pragma once

#include <stdint.h>
#include <time.h>
#include <NTPClient.h>
#include <WiFiUdp.h>

typedef struct {
	int32_t year;
	int32_t month;	// 1==January
	int32_t day;	// >= 1
	int32_t weekday;	// 1==Sunday
	int32_t hour;
	int32_t hour12;
	uint8_t isAM;
	int32_t minute;
	int32_t second;
} DateTime;

class DateTimeController {

public:
	DateTimeController();
	void init();
	const DateTime * updateTime();
	const DateTime * getTime();
	void printTime(char * buf);

protected:
	WiFiUDP ntpUDP;
	NTPClient * timeClient;
	time_t localTime;
	DateTime dateTime;
};
