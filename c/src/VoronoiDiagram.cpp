#include <Arduino.h>
#include <stdint.h>
#include <math.h>
#include "VoronoiDiagram.h"
#include "MathUtil.h"

// -1 == disable
#define FORCE_LAYOUT     (-1)

VoronoiDiagram::VoronoiDiagram()
{

}

// Choose a different layout semi-randomly
void VoronoiDiagram::newLayout(VPoint * vpts, uint8_t vptCount, int16_t width, int16_t height)
{
	uint8_t newLayout = ((uint8_t)(lastLayout) + random(1, 2));
	newLayout %= (uint8_t)(k_layout_MAX);

	lastLayout = (VLayout)(newLayout);

	if (0 <= (int8_t)(FORCE_LAYOUT)) {
		lastLayout = (VLayout)(FORCE_LAYOUT);
	}

	createLayout(vpts, vptCount, width, height, lastLayout);
}

void VoronoiDiagram::createLayout(VPoint * vpts, uint8_t vptCount, int16_t width, int16_t height, VLayout layout)
{
	switch (layout) {
		case k_layout_random:
		{
			for (uint8_t v = 0; v < vptCount; v++) {
				vpts[v].x = random(width);
				vpts[v].y = random(height);
			}
		}
		break;

		case k_layout_diagonal_line:
		{
			const int16_t dx = 20;
			const int16_t dy = 30;
			bool flip = (random(2) ? true : false);

			for (uint8_t v = 0; v < vptCount; v++) {
				float p = (float)(v) / (vptCount - 1);
				vpts[v].x = (width * p) + randBi(dx);
				vpts[v].y = (height * (flip ? (1.0f - p) : p)) + randBi(dy);
			}
		}
		break;

		case k_layout_ring:
		{
			// Circular placement around the center
			const int16_t dx = 25;
			const int16_t dy = 25;

			for (uint8_t v = 0; v < vptCount; v++) {
				float p = (float)(v) / (vptCount - 1);

				float angle = p * PI * 2.0f;
				float radius = width / 2;
				vpts[v].x = width / 2 + cosf(angle) * radius + randBi(dx);
				vpts[v].y = height / 2 + sinf(angle) * radius + randBi(dy);
			}
		}
		break;

		case k_layout_sediment:
		{
			// Originally the points were arranged in
			// large sine-wave shapes. It created regular,
			// boring grid-like tesselations.
			// Sediment version: Very low sine frequency,
			// points are clustered around a column.
			const int16_t dx = 20;
			const int16_t dy = 10;

			int16_t radius = random(width / 2, width);
			float phase = randf(PI * 2.0f);

			float freq = (PI * 2.0f) * randf(0.01f, 1.8f);

			// This is wrong, but I like how it looks much
			// better, it creates more abstract horizontals.
			freq /= vptCount;

			for (uint8_t v = 0; v < vptCount; v++) {
				float p = (float)(v) / (vptCount - 1);

				vpts[v].x = width / 2 + cosf(phase + freq * p) * radius + randBi(dx);
				vpts[v].y = height * p + randBi(dy);
			}
		}
		break;

		case k_layout_grid:
		{
			int16_t sz = random(width / 12, width / 5);
			int16_t xOffset = random(sz);
			int16_t yOffset = random(sz);
			int16_t across = (width - xOffset) / sz;
			int16_t down = (height - yOffset) / sz;

			for (uint8_t v = 0; v < vptCount; v++) {
				vpts[v].x = xOffset + random(across) * sz;
				vpts[v].y = yOffset + random(down) * sz;
			}
		}
		break;

		case k_layout_spiral:
		{
			const int16_t dx = 8;
			const int16_t dy = 8;

			float twist = randf(PI * 4, PI * 8) * (random(2) ? 1.0f : -1.0f);
			float phase = randf(PI * 2.0f);
			float perspective = randf(0.5f, 3.0f);

			float centerX = random(width);
			float centerY = random(height);

			// Distance to furthest corner
			int16_t maxX = MAX(centerX, width - centerX);
			int16_t maxY = MAX(centerY, height - centerY);
			float maxDist = sqrtf(maxX * maxX + maxY * maxY);

			for (uint8_t v = 0; v < vptCount; v++) {
				float p = (float)(v) / (vptCount - 1);

				float distance = powf(p, perspective) * maxDist;
				vpts[v].x = centerX + cosf(phase + twist * p) * distance + randBi(dx);
				vpts[v].y = centerY + sinf(phase + twist * p) * distance + randBi(dy);
			}
		}
		break;

		default: break;
	}

}

void VoronoiDiagram::scoochLayout(VPoint * vpts, uint8_t vptCount)
{
	const float maxDistance = 23;

	for (uint8_t v = 0; v < vptCount; v++) {
		float dist = randf(maxDistance);
		float angle = randf(PI * 2.0f);

		vpts[v].x += cosf(angle) * dist;
		vpts[v].y += sinf(angle) * dist;
	}
}
