// LED_BRIGHTNESS: Range [0..255]
#define LED_BRIGHTNESS            (255)

#define FASTLED_FORCE_SOFTWARE_SPI

#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <Arduino.h>
#include <WiFi.h>
#include <WiFiMulti.h>
#include <FastLED.h>
#include <Preferences.h>
#include <GxEPD2_BW.h>

#include "DateTimeController.h"
#include "UserSettings.h"
#include "http_server.h"

// LEDs
#include "LEDLayout.h"
#include "AnimSolids.h"

#include "MountainDisplay.h"

#include "WiFi_Credentials.h"

#define LED_PIN               (25)
//#define LEVEL_SHIFTER_OE_PIN  (19)

#define BLUE_LED_BUILTIN      (5)

Preferences preferences;
UserSettings settings;

/**
 * 2023-11-22 :: Troubleshooting kernal panic, on build/run in the
 * futuristic year of 2023. Is this because GxEPD2 is constructed as a
 * static instance in MountainDisplay.cpp ? Backtrace hints this may be
 * the issue. Maybe declaring it at the top/"global" level will fix this?
 * Similar to the Preferences declaration issue, from 2017 or whatever.
 */
GxEPD2_BW<GxEPD2_420, GxEPD2_420::HEIGHT> gxepd(GxEPD2_420(EPD_CS, EPD_DC, EPD_RST, EPD_BUSY));

uint32_t prev_millis = 0;
int32_t prev_mins = -1;
int32_t prev_sec = -1;

CRGB leds[FASTLED_COUNT];

// RGBW buffer
RGBW ledbuf[LED_COUNT];

//U8G2_FOR_ADAFRUIT_GFX u8g2Fonts;

DateTimeController dateTimeController;
MountainDisplay mountainDisplay;
AnimSolids anim;

float prevBrightness = -1.0f;
float brightness = 0.0f;


void updateTime()
{
	const DateTime * dt = dateTimeController.updateTime();

	// Production brightness: Based on wake time
	int32_t secondsSinceMidnight = (dt->hour * 3600L) + (dt->minute * 60L) + dt->second;

	// Seconds since wake started. Wrap around so it's positive.
	int32_t sinceWake = secondsSinceMidnight - settings.getWakeTimeInSeconds();
	if (sinceWake < 0) {
		sinceWake += 24L * 60L * 60L;	// + one day
	}

	int32_t fadeIn = settings.getFadeInSeconds();
	int32_t wakeDuration = settings.getWakeDurationSeconds();

	if (sinceWake < wakeDuration) {
		float ramp = sinceWake * (1.0f / wakeDuration);	// [0..1]
		ramp *= (float)(wakeDuration) / (float)(fadeIn);
		brightness = min(1.0f, max(0.0f, ramp));

	} else {
		brightness = 0.0f;	// off!
	}

	// DEV FIXME: Fixing the wake cycle
	// Blink the onboard blue LED for testing
	pinMode(5, OUTPUT);
	digitalWrite(5, (brightness > 0.0f) ? HIGH : LOW);

	if (dt->second != prev_sec) {
		prev_sec = dt->second;

		char buf[50];
		dateTimeController.printTime(buf);
		Serial.print(buf);
		Serial.print(" :: LED brightness: ");
		Serial.println(brightness);
	}

	// Update EPD once a minute
	if (prev_mins != dt->minute) {
		prev_mins = dt->minute;

		IPAddress address = WiFi.localIP();
		mountainDisplay.update(dt, &address);

		// At 12pm, if uptime is >2 hours: Reboot, to resync the clock.
		if ((dt->hour == 12) && (dt->minute == 0) && (millis() > (2L * 60L * 60L * 1000L))) {
			mountainDisplay.showMessage("Rebooting to sync the time...", true);

			delay(5000);	// Allow time to read "Rebooting" message
			ESP.restart();
			delay(1000);	// probably not needed
		}
	}
}

void setup()
{
	// Start FastLED
	LEDS.addLeds<WS2812, LED_PIN, RGB>(leds, FASTLED_COUNT);
	FastLED.setDither(0);	// Screw "temporal dithering"
	LEDS.setBrightness(LED_BRIGHTNESS);
	LEDS.clear();
	FastLED.show();

	pinMode(BLUE_LED_BUILTIN, OUTPUT);
	digitalWrite(BLUE_LED_BUILTIN, HIGH);	// active low

	preferences.begin("mtn");
	settings.init(&preferences);

	Serial.begin(115200);
	//while (!Serial) {}	// Wait for Serial monitor

	Serial.print("wakeHour:");
	Serial.print(settings.getWakeHour());
	Serial.print(" wakeMinute:");
	Serial.println(settings.getWakeMinute());

	mountainDisplay.init(&gxepd);

	WiFiMulti wifiMulti;
	for (uint32_t i = 0; i < CREDENTIALS_LEN; i++) {
		wifiMulti.addAP(CREDENTIALS[i].ssid, CREDENTIALS[i].password);
	}

	Serial.println("Connecting ");
	while (wifiMulti.run() != WL_CONNECTED) {
		Serial.print(".");
		delay(1000);
	}

	Serial.println("");
	Serial.println("WiFi connected");
	Serial.println("IP address: ");
	Serial.println(WiFi.localIP());

	// Get the time
	dateTimeController.init();

	http_server_setup(&dateTimeController, &mountainDisplay, &settings);

	// Show message with SSID and IP address
	char msg[MESSAGE_LEN + 1];
	sprintf(msg, "%s :: %s", WiFi.SSID().c_str(), WiFi.localIP().toString().c_str());
	const bool DRAW_NOW = false;
	mountainDisplay.showMessage(&msg[0], DRAW_NOW);
}

void loop()
{
	digitalWrite(BLUE_LED_BUILTIN, HIGH);	// active low. Stay dark plz

	http_server_handle();

	digitalWrite(BLUE_LED_BUILTIN, HIGH);	// active low. Stay dark plz

	uint32_t currentMillis = millis();
	uint16_t elapsed = currentMillis - prev_millis;
	prev_millis = currentMillis;

	// Only update LEDs when brightness is active, or changed.
	if ((prevBrightness != 0.0f) || (brightness != 0.0f)) {

		// Animations are cooked at 60 FPS
		float timeMult = elapsed * (60.0f / 1000.0f);

		// Prevent large skips when EPD refreshes
		timeMult = min(4.0f, timeMult);

		uint8_t ledCount = settings.getLedCount();

		anim.tick(timeMult, ledbuf, brightness, ledCount);

		// Convert ledbuf to the format expected by
		// the LED strip (GE60RGBW2912C, UCS2912)
		// Data format is: RGBRGBRGBWWW
		int8_t offset = 0;
		for (int8_t i = 0; i < LED_COUNT; i += 3) {
			for (int8_t j = 0; j < 3; j++) {
				leds[offset + j].r = ledbuf[i + j].r;
				leds[offset + j].g = ledbuf[i + j].g;
				leds[offset + j].b = ledbuf[i + j].b;
			}

			leds[offset + 3].r = ledbuf[i + 0].w;
			leds[offset + 3].g = ledbuf[i + 1].w;
			leds[offset + 3].b = ledbuf[i + 2].w;

			offset += 4;
		}

		FastLED.show();
	}

	prevBrightness = brightness;

	updateTime();

	digitalWrite(BLUE_LED_BUILTIN, HIGH);	// active low
}
