#include <stdbool.h>
#include <stdint.h>
#include <Preferences.h>

#include "esp_system.h"
#include "nvs_flash.h"
#include "nvs.h"

#include "UserSettings.h"
#include "LEDLayout.h"

const char * WAKE_HOUR_KEY = "wakeHour";
const char * WAKE_MINUTE_KEY = "wakeMinute";
const char * LED_COUNT_KEY = "ledCount";
const char * FADE_IN_SECONDS_KEY = "fadeInS";
const char * WAKE_DURATION_SECONDS_KEY = "wakeDurS";

UserSettings::UserSettings()
{
}

void UserSettings::init(Preferences * _prefs)
{
	prefs = _prefs;

	// Fall back on defaults
	wakeHour = prefs->getUChar(WAKE_HOUR_KEY, wakeHour);
	wakeMinute = prefs->getUChar(WAKE_MINUTE_KEY, wakeMinute);
	ledCount = prefs->getUChar(LED_COUNT_KEY, ledCount);
	fadeInSeconds = prefs->getLong(FADE_IN_SECONDS_KEY, fadeInSeconds);
	wakeDurationSeconds = prefs->getLong(WAKE_DURATION_SECONDS_KEY, wakeDurationSeconds);
}

uint8_t UserSettings::getWakeHour()
{
	return wakeHour;
}

uint8_t UserSettings::getWakeMinute()
{
	return wakeMinute;
}

int32_t UserSettings::getWakeTimeInSeconds()
{
	int32_t h = getWakeHour();
	int32_t m = getWakeMinute();
	return (h * 60 * 60) + (m * 60);
}

uint8_t UserSettings::getLedCount()
{
	return ledCount;
}

uint32_t UserSettings::getFadeInSeconds()
{
	return fadeInSeconds;
}

uint32_t UserSettings::getWakeDurationSeconds()
{
	return wakeDurationSeconds;
}

bool UserSettings::setWakeTime(uint8_t hour, uint8_t minute)
{
	if ((24 <= hour) || (60 <= minute)) {
		return false;
	}

	wakeHour = hour;
	wakeMinute = minute;

	size_t sz_h = prefs->putUChar(WAKE_HOUR_KEY, hour);
	size_t sz_m = prefs->putUChar(WAKE_MINUTE_KEY, minute);

	return (sz_h > 0) && (sz_m > 0);
}

bool UserSettings::setLedCount(uint8_t _ledCount)
{
	if (LED_COUNT < _ledCount) {
		return false;
	}

	ledCount = _ledCount;

	size_t sz = prefs->putUChar(LED_COUNT_KEY, ledCount);

	return (sz > 0);
}

bool UserSettings::setFadeInSeconds(int32_t seconds)
{
	if ((seconds < 0) || ((24 * 60 * 60) < seconds)) {
		return false;
	}

	fadeInSeconds = seconds;

	size_t sz = prefs->putLong(FADE_IN_SECONDS_KEY, seconds);

	return (sz > 0);
}

bool UserSettings::setWakeDurationSeconds(int32_t seconds)
{
	if ((seconds < 0) || ((24 * 60 * 60) < seconds)) {
		return false;
	}

	wakeDurationSeconds = seconds;

	size_t sz = prefs->putLong(WAKE_DURATION_SECONDS_KEY, seconds);

	return (sz > 0);
}
