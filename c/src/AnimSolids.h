#pragma once

#include "LEDLayout.h"

#define SOLID_COUNT              (3)

typedef struct {
	RGBW start;
	RGBW color;	// Current color
	RGBW end;
	float progress;
	float speed;
	float delay;
} Solid;

class AnimSolids {

public:

	AnimSolids();

	void tick(float timeMult, RGBW * ledbuf, float brightness, uint8_t ledCount);

protected:

	Solid solids[SOLID_COUNT];

};
