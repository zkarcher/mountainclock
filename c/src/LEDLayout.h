#pragma once

//
//  Mountain LED layout
//
// 2019-07-17 ZKA:
// This file is _not_ being used with the live-coding
// framework. Because that framework is broken.
//

#include <stdint.h>
#include "MathUtil.h"

// Actual number of physical RGBW LED modules:
#define LED_COUNT                 (57)

// Internally, FastLED treats these as RGB,
// so we allocate more memory:
#define FASTLED_COUNT             (57 + 19)

// RGBW range: [0..255]
typedef struct {
	float r;
	float g;
	float b;
	float w;
} RGBW;

inline void lerpRGBW(const RGBW * c0, const RGBW * c1, const float p, RGBW * dest)
{
	dest->r = lerp(c0->r, c1->r, p);
	dest->g = lerp(c0->g, c1->g, p);
	dest->b = lerp(c0->b, c1->b, p);
	dest->w = lerp(c0->w, c1->w, p);
}

inline void copyRGBW(RGBW * dest, const RGBW * src)
{
	dest->r = src->r;
	dest->g = src->g;
	dest->b = src->b;
	dest->w = src->w;
}

inline void apply0xff(RGBW * rgbw) {
	rgbw->r *= 255.0f;
	rgbw->g *= 255.0f;
	rgbw->b *= 255.0f;
	rgbw->w *= 255.0f;
}

inline void applyGamma0xff(RGBW * rgbw) {
	rgbw->r = (rgbw->r * rgbw->r) * 255.0f;
	rgbw->g = (rgbw->g * rgbw->g) * 255.0f;
	rgbw->b = (rgbw->b * rgbw->b) * 255.0f;
	rgbw->w = (rgbw->w * rgbw->w) * 255.0f;
}

typedef struct {
	int8_t mountain;	// 0==left, 1==big back, 2==right
	int8_t x;
	int8_t y;
	int8_t wakeOrder;
} LedPos;

// mountain, x, y
const LedPos LED_LAYOUT[] = {
	// Right
	{2, 12, 4, 39},	// Front strip
	{2, 13, 4, 1},
	{2, 14, 4, 31},

	{2, 14, 3, 16},	// Back strip
	{2, 13, 3, 9},
	{2, 12, 3, 25},

	// Big mountain in the middle-background
	{1, 14, 2, 55},	// Front strip
	{1, 13, 2, 49},
	{1, 12, 2, 45},
	{1, 11, 2, 35},
	{1, 10, 2, 29},
	{1,  9, 2, 27},
	{1,  8, 2, 23},
	{1,  7, 2, 22},
	{1,  6, 2, 24},
	{1,  5, 2, 28},
	{1,  4, 2, 30},
	{1,  3, 2, 36},
	{1,  2, 2, 46},
	{1,  1, 2, 50},
	{1,  0, 2, 56},

	{1,  0, 1, 51},	// Center strip
	{1,  1, 1, 43},
	{1,  2, 1, 37},
	{1,  3, 1, 20},
	{1,  4, 1, 7},
	{1,  5, 1, 5},
	{1,  6, 1, 3},
	{1,  7, 1, 0},
	{1,  8, 1, 4},
	{1,  9, 1, 6},
	{1, 10, 1, 8},
	{1, 11, 1, 21},
	{1, 12, 1, 38},
	{1, 13, 1, 44},
	{1, 14, 1, 52},

	{1, 14, 0, 53},	// Back strip
	{1, 13, 0, 47},
	{1, 12, 0, 41},
	{1, 11, 0, 33},
	{1, 10, 0, 18},
	{1,  9, 0, 14},
	{1,  8, 0, 12},
	{1,  7, 0, 11},
	{1,  6, 0, 13},
	{1,  5, 0, 15},
	{1,  4, 0, 19},
	{1,  3, 0, 34},
	{1,  2, 0, 42},
	{1,  1, 0, 48},
	{1,  0, 0, 54},

	// Left
	{0,  0, 3, 17},	// Back strip
	{0,  1, 3, 10},
	{0,  2, 3, 26},

	{0,  2, 4, 40},	// Front strip
	{0,  1, 4, 2},
	{0,  0, 4, 32},

};
