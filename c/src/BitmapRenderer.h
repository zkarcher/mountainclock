/*
 * @author zkarcher
 */

#pragma once

#include <stdint.h>
#include <stdbool.h>
#include <Adafruit_GFX.h>
#include <GxEPD2_BW.h>
//#include "vendor/GxEPD2/GxEPD2_BW.h"
#include "VLQDecoder.h"

typedef enum {
	k_align_none = 0,
	k_align_left = 1,
	k_align_center = 2,
	k_align_right = 4,
	/*
	// FIXME: Not supported yet
	k_align_baseline = 8,
	k_align_vcenter = 16,
	*/
	k_align_hanging = 32
} TextAlign;

typedef struct codepoint {
	int16_t xOffset;
	int16_t yOffset;
	uint32_t xAdvance;
	uint32_t width;
	uint32_t height;
} Codepoint;

class BitmapRenderer {

public:

	BitmapRenderer(Adafruit_GFX * inGfx);

	void drawVLQBitmap(VLQDecoder * decoder, int16_t atX, int16_t atY, uint16_t color);

	void drawText(
		const uint8_t * fontData,
		const char * string,
		int16_t atX = 0,
		int16_t atY = 0,
		uint8_t align = k_align_left | k_align_hanging,
		int16_t letterSpacing = 0,
		uint16_t color = GxEPD_BLACK
	);

	void drawTextOutline(
		const uint8_t * fontData,
		const char * string,
		int16_t atX = 0,
		int16_t atY = 0,
		uint8_t align = k_align_left | k_align_hanging,
		int16_t letterSpacing = 0,
		uint16_t fillColor = GxEPD_WHITE,
		uint16_t outlineColor = GxEPD_BLACK,
		int8_t outlineStroke = 3
	);

protected:

	Adafruit_GFX * gfx;

	bool findCodepoint(Codepoint * codep, VLQDecoder * decoder, const uint8_t * fontData, uint32_t ch);

};
