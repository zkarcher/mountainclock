#include <stdint.h>
#include "FastFloat.h"

// @author zkarcher
//
// Multiplies x by 0xff, clamps inside 0..0xff, returns as uint8_t.
// This may be faster than standard `constrain(f * 255.0f, 0.0f, 255.0f)`
// by a factor of ~4!
//
// Output values may not precisely match f * 255.0f, but they're close.
// Run the FloatTest.ino sketch to see this in action.
//
// Teensy 3.2:
//     standard time: 596 ms
//         fast time: 154 ms

uint8_t fastFloat_x_0xff(float x) {
	// Assuming IEEE-754 floats, see:
	// https://www.h-schmidt.net/FloatConverter/IEEE754.html
	// Bits: 1 sign, 8 exponent, 23 mantissa

	// direct translation of float bits to uint32_t, via pointers
	uint32_t i32 = *(uint32_t*)&x;

	// We only need the top 16 bits of the float.
	uint16_t i = (i32 >> 16);

	// Negative?
	if (i & 0x8000) {
		return 0;
	}

	// Shift off the mantissa, to get the exponent.
	uint8_t exponent = i >> 7;

	// Too high? (>= 1.0f)
	if (exponent >= 0b01111111) {
		return 0xff;
	}

	// Too low? (Too close to zero?)
	if (exponent < 0b01110111) {
		return 0;
	}

	// Determine the highest bit that will be set.
	// The lowest 4 bits of exponent (inverted) are the number
	// of bits to right-shift our highest possible bit (0x80) down.
	uint8_t shift = ((~exponent) & 0x0f) - 1;

	// We only need 7 bits of mantissa
	uint8_t mantissa = i;// & 0x7f;

	return ((0x80 | mantissa) >> shift);
}
