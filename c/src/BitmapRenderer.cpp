#include <stdint.h>
#include <stdbool.h>
#include <Adafruit_GFX.h>
#include "BitmapRenderer.h"
#include "VLQDecoder.h"

BitmapRenderer::BitmapRenderer(Adafruit_GFX * inGfx)
{
	gfx = inGfx;
}

void BitmapRenderer::drawVLQBitmap(VLQDecoder * decoder, int16_t atX, int16_t atY, uint16_t color)
{
	// Read: width, height, draw direction
	uint32_t width = decoder->decodeUint32();
	uint32_t height = decoder->decodeUint32();
	uint8_t dir = (uint8_t)(decoder->decodeUint32());
	uint32_t wordSize = decoder->decodeUint32();
	decoder->setWordSize(wordSize);

	// Start drawing pixels
	uint32_t x = 0;
	uint32_t y = 0;
	int32_t offRun = 0;
	int32_t onRun = 0;
	bool isOn = false;
	bool hasDrawnOff = false;
	bool hasDrawnOn = false;

	/*
	// Testing
	gfx->fillCircle(50, 50, 50, 0);	// black
	gfx->fillCircle(150, 50, 50, 1);	// white
	gfx->fillCircle(250, 50, 50, 0xff);	// also white
	*/

	while (true) {
		int32_t steps;

		if (isOn) {
			if (hasDrawnOn) {
				int32_t delta = decoder->decodeZigZagInt32();
				onRun += delta;

			} else {
				onRun = decoder->decodeUint32(); // First value is unsigned
				hasDrawnOn = true;
			}

			steps = onRun;

		} else {
			if (hasDrawnOff) {
				int32_t delta = decoder->decodeZigZagInt32();
				offRun += delta;

			} else {
				offRun = decoder->decodeUint32();	// First value is unsigned
				hasDrawnOff = true;
			}

			steps = offRun;
		}

		for (int32_t s = 0; s < steps; s++) {
			if (isOn) {
				gfx->drawPixel(atX + x, atY + y, color);
			}

			if (dir == 1) {
				// Across
				x++;
				if (width <= x) {
					x = 0;
					y++;

					if (height <= y) {
						return;	// error, out of bounds
					}
				}

			} else {
				// Down
				y++;
				if (height <= y) {
					y = 0;
					x++;

					if (width <= x) {
						return;	// error, out of bounds
					}
				}
			}
		}

		if (decoder->isDecodeFinished()) {
			return;
		}

		// Switch phases
		isOn = !isOn;
	}
}

// Only supports ASCII, for now.
void BitmapRenderer::drawText(const uint8_t * fontData, const char * string, int16_t atX, int16_t atY, uint8_t align, int16_t letterSpacing, uint16_t color)
{
	Codepoint codep;

	// Align center/right: Need to compute overall
	// text width, and adjust atX
	if ((align & k_align_center) || (align & k_align_right)) {
		int16_t totalWidth = 0;

		// Measure each character
		const char * str = string;

		while ((*str) != '\0') {
			uint8_t ch = (*str);

			VLQDecoder decoder;

			if (findCodepoint(&codep, &decoder, fontData, ch)) {
				totalWidth += codep.xAdvance + letterSpacing;
			}

			str++;
		}

		// Final character has whitespace on the right,
		// so subtract that.
		totalWidth -= (codep.xAdvance - codep.width) - codep.xOffset;

		if (align & k_align_right) {
			atX -= totalWidth;
		} else if (align & k_align_center) {
			atX -= (totalWidth / 2);
		}
	}

	while ((*string) != '\0') {
		// Character to render
		uint8_t ch = (*string);

		VLQDecoder decoder;

		if (findCodepoint(&codep, &decoder, fontData, ch)) {
			// decoder is now ready to decode & draw bitmap.
			drawVLQBitmap(&decoder, atX + codep.xOffset, atY + codep.yOffset, color);

			atX += codep.xAdvance + letterSpacing;
		}

		string++;	// Next character plz
	}
}

void BitmapRenderer::drawTextOutline(const uint8_t * fontData, const char * string, int16_t atX, int16_t atY, uint8_t align, int16_t letterSpacing, uint16_t fillColor, uint16_t outlineColor, int8_t outlineStroke)
{
	// FIXME: Draw better lines for different outlineStroke's

	// 2px corners, 3px sides

	for (int8_t i = -2; i <= 2; i += 4) {
		for (int8_t j = -2; j <= 2; j += 4) {
			drawText(fontData, string, atX + i, atY + j, align, letterSpacing, outlineColor);
		}
	}

	for (int8_t i = -outlineStroke; i <= outlineStroke; i += outlineStroke * 2) {
		drawText(fontData, string, atX + i, atY, align, letterSpacing, outlineColor);
	}

	for (int8_t j = -outlineStroke; j <= outlineStroke; j += outlineStroke * 2) {
		drawText(fontData, string, atX, atY + j, align, letterSpacing, outlineColor);
	}

	drawText(fontData, string, atX, atY, align, letterSpacing, fillColor);
}

// Returns true if found
bool BitmapRenderer::findCodepoint(Codepoint * codep, VLQDecoder * decoder, const uint8_t * fontData, uint32_t ch)
{
	const uint8_t * dat = fontData;

	while ((*dat) != 0) {	// 0 == terminator, codepoint not found in block.

		// Get block size: Quick 7-bit VLQ-like routine.
		// MSB bit is set: Then the value continues.
		uint32_t blockSize = 0;
		uint8_t datRead = 0;
		do {
			datRead = (*dat++);
			blockSize = (blockSize << 7) | (datRead & 0x7f);
		} while (datRead & 0x80);

		const uint8_t * blockData = dat;
		const uint8_t * blockEnd = blockData + blockSize;

		// Not our char? Skip to the next block.
		uint8_t thisChar = (*blockData++);
		if (thisChar != ch) {
			dat = blockEnd;
			continue;
		}

		VLQDecoder vlq(blockData, blockEnd, 3);

		codep->xOffset = (int16_t)(vlq.decodeZigZagInt32());
		codep->yOffset = (int16_t)(vlq.decodeZigZagInt32());
		codep->xAdvance = vlq.decodeUint32();

		// vlq is in position for drawVLQBitmap.
		// Pass this back to the text drawing routine.
		decoder->copy(&vlq);

		codep->width = vlq.decodeUint32();
		codep->height = vlq.decodeUint32();

		return true;
	}

	return false;
}
