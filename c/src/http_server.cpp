#include <stdbool.h>
#include <stdint.h>
#include <string.h>
#include <Arduino.h>
#include <WiFiClient.h>

#ifdef ESP8266
#include <ESP8266WebServer.h>
#else
#include <WebServer.h>
#endif

#include "http_server.h"
#include "DateTimeController.h"
#include "MountainDisplay.h"
#include "UserSettings.h"
#include "LEDLayout.h"

static WiFiClient client;
static DateTimeController * _dtc;
static MountainDisplay * _display;
static UserSettings * _settings;

#ifdef ESP8266
static ESP8266WebServer server(80);
#else
static WebServer server(80);
#endif

char clientMsg[MESSAGE_LEN + 1];

// most web requests are handled from the root page
// requests look like:
//  /set_wake?h=7&m=30

// Forward declarations
static void handle_root(),
	handle_set_wake(),
	handle_set_led_count(),
	handle_set_fade_in(),
	handle_set_wake_duration(),
	handle_restart();

void http_server_setup(DateTimeController * dtc, MountainDisplay * display, UserSettings * settings) {
	_dtc = dtc;
	_display = display;
	_settings = settings;

	server.on("/", handle_root);
	server.on("/set_wake", handle_set_wake);
	server.on("/set_led_count", handle_set_led_count);
	server.on("/set_fade_in", handle_set_fade_in);
	server.on("/set_wake_duration", handle_set_wake_duration);

	// Infinite loop, caused by page refreshing in browser:
	//server.on("/restart", []() { ESP.restart(); });

	server.on("/restart", handle_restart);

	server.onNotFound([]() {
		server.send(404, "text/plain", "File not found");
	});

	server.begin();
}

void http_server_handle() {
	server.handleClient();
}

static void handle_root() {
	byte mac_address[6];
	WiFi.macAddress(mac_address);

	String page = "<html><head>"
	"<meta charset='utf-8'>"
	"<meta name='viewport' content='width=device-width'>"
	"<title>Mountain Clock Controls</title>"
	"<style>"
	"body {"
	"    background-color: black;"
	"    font-family: sans-serif;"
	"}"
	"h1, h2, label, p, a {"
	"    color: #b4b4b4;"
	"}"
	"p.message {"
	"    color: rgb(255, 200, 100);"
	"    background-color: rgba(255, 200, 100, 0.25);"
	"    padding: 8pt 14pt;"
	"}"
	"</style>"
	"</head>"

	"<body>"
	"<h1>Mountain Clock Controls :) &#x26F0; &#x1F558;</h1>";

	if (clientMsg[0] != '\0') {
		page += "<p class='message'>";
		page += String(clientMsg);
		page += "</p>";

		// Clear the client message now.
		clientMsg[0] = '\0';
	}

	page +=

	//
	// -- WAKE TIME --
	//

	"<hr/><form action='/set_wake'>"

	// Wake time selectors
	"<h2>Wake time</h2>"

	"<label for='hour'>Hour</label> "
	"<select name='hour' id='hour'>";
	for (uint8_t i = 0; i < 24; i++) {
		page += "<option value='";
		page += String(i);
		if (i == _settings->getWakeHour()) {
			page += "' selected>";
		} else {
			page += "'>";
		}
		page += String(i);
		page += "</option>";
	}
	page += "</select><br/>"

	"<label for='minute'>Minute</label> "
	"<select name='minute' id='minute'>";
	for (uint8_t i = 0; i < 60; i += 5) {
		page += "<option value='";
		page += String(i);
		if (i == _settings->getWakeMinute()) {
			page += "' selected>";
		} else {
			page += "'>";
		}

		// Pad minutes with zeros
		char buf[10];
		sprintf(buf, "%02u", i);

		page += String(buf);
		page += "</option>";
	}
	page += "</select>"
	"<br/>"
	"<input type='submit' value='Set Wake Time'></input>"
	"</form>"

	//
	// -- LED COUNT --
	//

	"<hr/><form action='/set_led_count'>"

	// Wake time selectors
	"<h2>LED count</h2>"

	"<select name='leds' id='leds'>";
	for (uint8_t i = 0; i <= LED_COUNT; i++) {
		page += "<option value='";
		page += String(i);
		if (i == _settings->getLedCount()) {
			page += "' selected>";
		} else {
			page += "'>";
		}
		page += String(i);
		page += "</option>";
	}
	page += "</select><br/>"
	"<br/>"
	"<input type='submit' value='Set LED count'></input>"
	"</form>"

	//
	// -- FADE IN --
	//

	"<hr/><form action='/set_fade_in'>"

	// Wake time selectors
	"<h2>Fade In</h2>"

	"<select name='fadeIn' id='fadeIn'>";

	const int32_t DURATIONS[] = {1, 5, 10, 30, 60, 2*60, 3*60, 5*60, 10*60, 15*60, 20*60, 30*60, 45*60, 60*60, 2*60*60, 3*60*60, 6*60*60, 12*60*60, 24*60*60};
	const int32_t DURATIONS_LEN = sizeof(DURATIONS) / sizeof(*DURATIONS);

	for (int32_t i = 0; i < DURATIONS_LEN; i++) {
		int32_t dur = DURATIONS[i];
		page += "<option value='";
		page += String(dur);
		if (dur == _settings->getFadeInSeconds()) {
			page += "' selected>";
		} else {
			page += "'>";
		}

		if (dur < 60) {
			page += String(dur) + "s";
		} else if (dur < (60 * 60)) {
			page += String(dur / 60) + "m";
		} else {
			page += String(dur / (60 * 60)) + "h";
		}

		page += "</option>";
	}
	page += "</select><br/>"
	"<br/>"
	"<input type='submit' value='Set fade-in'></input>"
	"</form>"

	//
	// -- WAKE DURATION --
	//

	"<hr/><form action='/set_wake_duration'>"

	// Wake time selectors
	"<h2>Wake duration</h2>"

	"<select name='wakeDur' id='wakeDur'>";

	for (int32_t i = 0; i < DURATIONS_LEN; i++) {
		int32_t dur = DURATIONS[i];
		page += "<option value='";
		page += String(dur);
		if (dur == _settings->getWakeDurationSeconds()) {
			page += "' selected>";
		} else {
			page += "'>";
		}

		if (dur < 60) {
			page += String(dur) + "s";
		} else if (dur < (60 * 60)) {
			page += String(dur / 60) + "m";
		} else {
			page += String(dur / (60 * 60)) + "h";
		}

		page += "</option>";
	}
	page += "</select><br/>"
	"<br/>"
	"<input type='submit' value='Set wake duration'></input>"
	"</form>"

	//
	// -- RESTART --
	//

	"<hr/><a href='/restart'>Restart</a><hr/>";

	//
	// -- STATUS INFO --
	//

	char timeBuf[50];
	_dtc->printTime(timeBuf);

	//"<b>Hostname:</b> " + String(ssid) +
	page += "<p>"
	"<b>Page generated at:</b> " + String(timeBuf) +
	"<br/><b>IP address:</b> " + String(WiFi.localIP()[0]) + "."  + String(WiFi.localIP()[1]) + "."  + String(WiFi.localIP()[2]) + "."  + String(WiFi.localIP()[3]) +

	"<br/><b>MAC address:</b> " + String(mac_address[0], 16) + ":" + String(mac_address[1], 16) + ":" + String(mac_address[2], 16) + ":" + String(mac_address[3], 16) + ":" + String(mac_address[4], 16) + ":" + String(mac_address[5], 16) +

	"<br/><b>Wifi network:</b> " + WiFi.SSID() +
	"<br/><b>RSSI</b>: " + String(WiFi.RSSI()) +
	"<br/><b>subnet mask:</b> " + String(WiFi.subnetMask()[0]) + "."  + String(WiFi.subnetMask()[1]) + "."  + String(WiFi.subnetMask()[2]) + "."  + String(WiFi.subnetMask()[3]) +
	"<br/><b>default router:</b> " + String(WiFi.gatewayIP()[0]) + "."  + String(WiFi.gatewayIP()[1]) + "."  + String(WiFi.gatewayIP()[2]) + "."  + String(WiFi.gatewayIP()[3]) +
	"</p>"
	"</body>"
	"</html>";

	server.send(200, "text/html", page);
}

static void show_message_and_redirect_to_root()
{
	// Show this message on the display, too.
	const bool DRAW_NOW = true;
	_display->showMessage(clientMsg, DRAW_NOW);

	Serial.println(clientMsg);

	server.sendHeader("Location", "/");
	server.send(302);
}

static void handle_set_led_count() {
	bool success = false;

	long int count;

	if (server.hasArg("leds")) {
		count = strtol(server.arg("leds").c_str(), NULL, 10);	// base 10

		if ((0 <= count) && (count <= LED_COUNT)) {
			success = _settings->setLedCount((uint8_t)(count));
		}
	}

	if (success) {
		sprintf(clientMsg, "Set LED count: %u", count);
	} else {
		sprintf(clientMsg, "** Invalid LED count");
	}

	show_message_and_redirect_to_root();
}

static void handle_set_wake() {
	bool success = false;

	long int h, m;

	if (server.hasArg("hour") && server.hasArg("minute")) {
		h = strtol(server.arg("hour").c_str(), NULL, 10);	// base 10
		m = strtol(server.arg("minute").c_str(), NULL, 10);	// base 10

		if ((0 <= h) && (h < 24) && (0 <= m) && (m < 60)) {
			success = _settings->setWakeTime((uint8_t)(h), (uint8_t)(m));
		}
	}

	if (success) {
		sprintf(clientMsg, "Wake time is set: %02u:%02u", h, m);
	} else {
		sprintf(clientMsg, "** Invalid wake time");
	}

	show_message_and_redirect_to_root();
}

static void handle_set_fade_in() {
	bool success = false;

	long int fadeIn;

	if (server.hasArg("fadeIn")) {
		fadeIn = strtol(server.arg("fadeIn").c_str(), NULL, 10);	// base 10

		if ((0 <= fadeIn) && (fadeIn <= (24 * 60 * 60))) {
			success = _settings->setFadeInSeconds(fadeIn);
		}
	}

	if (success) {
		sprintf(clientMsg, "Set fade-in: %u seconds", fadeIn);
	} else {
		sprintf(clientMsg, "** Invalid fade-in seconds");
	}

	show_message_and_redirect_to_root();
}

static void handle_set_wake_duration() {
	bool success = false;

	long int wakeDur;

	if (server.hasArg("wakeDur")) {
		wakeDur = strtol(server.arg("wakeDur").c_str(), NULL, 10);	// base 10

		if ((0 <= wakeDur) && (wakeDur <= (24 * 60 * 60))) {
			success = _settings->setWakeDurationSeconds(wakeDur);
		}
	}

	if (success) {
		sprintf(clientMsg, "Set wake duration: %u seconds", wakeDur);
	} else {
		sprintf(clientMsg, "** Invalid wake duration");
	}

	show_message_and_redirect_to_root();
}

static void handle_restart() {
	server.sendHeader("Location", "/");
	server.send(302);

	delay(500);

	ESP.restart();
}
