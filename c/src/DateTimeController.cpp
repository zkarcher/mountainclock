#include <stdint.h>
#include <string.h>
#include <time.h>
#include <NTPClient.h>
#include <TimeLib.h>
#include <Timezone.h>
#include "DateTimeController.h"

#define NTP_ADDRESS   "pool.ntp.org"

// US Pacific Time Zone (Portland Oregon)
TimeChangeRule ptDST = {"PDT", Second, Sun, Mar, 2, -420};  // Daylight time = UTC - 7 hours
TimeChangeRule ptSTD = {"PST", First, Sun, Nov, 2, -480};   // Standard time = UTC - 8 hours
Timezone timeRule(ptDST, ptSTD);

DateTimeController::DateTimeController()
{
}

void DateTimeController::init()
{
	// NTP_INTERVAL: Super-long, so time never re-updates from server
	const long ntpInterval = 5L * 24L * 60L * 60L * 1000L;	// in milliseconds
	const long initialNTP_offset = 0;

	timeClient = new NTPClient(ntpUDP, NTP_ADDRESS, initialNTP_offset, ntpInterval);

	timeClient->begin();

	Serial.println("timeClient->forceUpdate");
	while(!timeClient->forceUpdate()) {
		Serial.print(".");
	}
	Serial.println("");
}

const DateTime * DateTimeController::updateTime()
{
	time_t utc = timeClient->getEpochTime();
	localTime = timeRule.toLocal(utc);

	dateTime = (DateTime){
		year: year(localTime),
		month: month(localTime),
		day: day(localTime),
		weekday: weekday(localTime),
		hour: hour(localTime),
		hour12: hourFormat12(localTime),
		isAM: isAM(localTime),
		minute: minute(localTime),
		second: second(localTime)
	};

	return &dateTime;
}

const DateTime * DateTimeController::getTime()
{
	return &dateTime;
}

void DateTimeController::printTime(char * buf)
{
	sprintf(buf, "%i-%02i-%02i %02i:%02i:%02i",
		dateTime.year,
		dateTime.month,
		dateTime.day,
		dateTime.hour,
		dateTime.minute,
		dateTime.second
	);
}
