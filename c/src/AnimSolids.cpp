#include <stdint.h>
#include <Arduino.h>
#include "AnimSolids.h"
#include "LEDLayout.h"
#include "MathUtil.h"

#define COLOR_FADE_SPEED        (0.002f)

// Delays: in frames
#define COLOR_MIN_DELAY         (60.0f)
#define COLOR_MAX_DELAY         (180.0f)

AnimSolids::AnimSolids()
{
	for (int8_t s = 0; s < SOLID_COUNT; s++) {
		Solid * solid = &solids[s];
		solid->start = {.r = 0, .g = 0, .b = 0, .w = 0};
		solid->end = {.r = 0, .g = 0, .b = 0, .w = 0};
		solid->speed = 1.0f;
		solid->progress = 1.0f;
		solid->delay = 0.0f;
	}
}

void AnimSolids::tick(float timeMult, RGBW * ledbuf, float brightness, uint8_t ledCount)
{
	// Tick each solid
	for (int8_t s = 0; s < SOLID_COUNT; s++) {
		Solid * solid = &solids[s];

		if (solid->delay > 0.0f) {
			solid->delay -= timeMult;
		} else {
			solid->progress += solid->speed * timeMult;
		}

		if (solid->progress >= 1.0f) {

			// Reset progress
			solid->progress = 0.0f;

			// Choose a random delay
			solid->delay = randf(COLOR_MIN_DELAY, COLOR_MAX_DELAY);

			// Choose a random speed
			solid->speed = randf(1.0f, 2.0f) * COLOR_FADE_SPEED;

			// Previous end color is now the start color
			copyRGBW(&solid->start, &solid->end);

			// Choose a color mix:
			// G, (B), GB, W, BW, GBW, RGBW
			// (parens: small-mountains only)
			const RGBW COLORS[] = {
				{.r = 0.0, .g = 1.0, .b = 0.0, .w = 0.0},	// G
				{.r = 0.0, .g = 1.0, .b = 1.0, .w = 0.0},	// GB
				{.r = 0.0, .g = 0.0, .b = 0.0, .w = 1.0},	// W
				{.r = 0.0, .g = 0.0, .b = 1.0, .w = 1.0},	// BW
				{.r = 0.0, .g = 1.0, .b = 1.0, .w = 1.0},	// GBW
				{.r = 1.0, .g = 1.0, .b = 1.0, .w = 1.0},	// RGBW
				{.r = 0.0, .g = 0.0, .b = 1.0, .w = 0.0},	// B
			};

			// Large mountain: Always show colors with white.
			int8_t choices = (s == 1) ? 6 : 7;
			int8_t rando = random(choices);

			copyRGBW(&solids[s].end, &COLORS[rando]);

		}	// !if (solid->progress >= 1.0f)

		// Blend the colors
		lerpRGBW(&solid->start, &solid->end, solid->progress, &solid->color);

		//applyGamma0xff(&solid->color);
		apply0xff(&solid->color);
	}

	// Cheapo "gamma" correction
	float bright2 = brightness * brightness;

	// Multiplier for LED wakeOrder
	float brightCountMult = bright2 * ledCount;

	for (int8_t i = 0; i < LED_COUNT; i++) {
		int8_t mountain = LED_LAYOUT[i].mountain;

		RGBW * color = &solids[mountain].color;

		int8_t order = LED_LAYOUT[i].wakeOrder;

		// Turn on wakeOrder 0 first
		float brt = brightCountMult - order;
		brt = min(1.0f, max(0.0f, brt));

		//copyRGBW(&ledbuf[i], &solids[mountain].color);
		ledbuf[i].r = color->r * brt;
		ledbuf[i].g = color->g * brt;
		ledbuf[i].b = color->b * brt;
		ledbuf[i].w = color->w * brt;
	}

}
