/*
 * @author zkarcher
 */

#pragma once

#include <cstddef>
#include <stdint.h>

class VLQDecoder {

public:

	VLQDecoder(const uint8_t * inStream = NULL, const uint8_t * inStreamEnd = NULL, uint8_t inWordSize = 3);

	const uint8_t * getStream();
	void setStream(const uint8_t * inStream, const uint8_t * inStreamEnd, uint8_t inWordSize);

	const uint8_t * getStreamEnd();
	uint8_t getBitMask();
	uint8_t getWordSize();

	// Known issue: Does not copy repeat loops/wordCount
	void copy(VLQDecoder * aDecoder);

	void setWordSize(uint8_t inWordSize);
	uint32_t decodeUint32();
	int32_t decodeZigZagInt32();
	bool isDecodeFinished();

private:
	const uint8_t * stream;
	const uint8_t * streamEnd;
	uint8_t wordSize;
	uint8_t bitMask;

	uint32_t repeatLoopsRemain;
	uint32_t repeatWordCount;
	const uint8_t * repeatStart;
	uint8_t repeatBitMask;
	uint32_t repeatWordsRemain;

	inline uint8_t getBit();

};
