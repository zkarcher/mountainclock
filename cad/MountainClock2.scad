$fn = 32;
HAIRLINE = 0.01;

WALL = 1.8 + 0.2;

BASE_HEIGHT = 2.0;
BASE_INSET = 10.0;

CORD_R = 7;
CORD_Z = 15;

SCREW_R = 1.8;
SCREEN_W = 65;
SCREEN_H = 86;
SCREEN_CORNER_R = 1.8;	// Display: rounded corners
SCREEN_X = 0;
SCREEN_Y = 12;	// From the bottom

function flipX(pt) = [-pt[0], pt[1]];
function flipY(pt) = [pt[0], -pt[1]];
function flipXY(pt) = [-pt[0], -pt[1]];

// lFront, etc: '0' falls back on front & side values.
function octoBase(front, side, lFront, lSide, rFront, rSide) = [
	flipY(rFront ? rFront : front),
	flipXY(lFront ? lFront : front),
	flipXY(lSide ? lSide : side),
	flipX(lSide ? lSide : side),
	flipX(lFront ? lFront : front),
	rFront ? rFront : front,
	rSide ? rSide : side,
	flipY(rSide ? rSide : side)
];

// Global mountain scale
SC = 0.94;

H_MULT = 1.15;
MOUNTAIN_HEIGHTS = [
	65 * H_MULT * SC,
	160 * H_MULT * SC,
	85 * H_MULT * SC,
	55 * H_MULT * SC
];

MOUNTAIN_POS = [
	[-113 * SC, -20 * SC, 0],
	[   0 * SC,  32 * SC, 0],
	[ 105 * SC, -17 * SC, 0],
	[-88 * SC, -20 * SC, 0]
];

FRONT0 = [34 * SC, 29 * SC];
SIDE0 = [52 * SC, 13 * SC]; 

FRONT1 = [115 * SC, 53 * SC];
SIDE1 = [160 * SC, 30 * SC];

FRONT2 = [45 * SC, 31 * SC];
SIDE2 = [65 * SC, 14 * SC];

FRONT0B = [29 * SC, 29 * SC];
SIDE0B = [47 * SC, 13 * SC]; 

MOUNTAIN_POLYS = [
	octoBase(FRONT0, SIDE0, 0, 0, [10, FRONT0[1]], [20, SIDE0[1]]),
	octoBase(FRONT1, SIDE1, 0, 0, 0, 0),
	octoBase(FRONT2, SIDE2, 0, 0, 0, 0),
	octoBase(FRONT0B, SIDE0B, [10, FRONT0B[1]], [20, SIDE0B[1]], 0, 0),
];

MOUNTAIN_COUNT = len(MOUNTAIN_HEIGHTS);

// Change 2D point to 3D
function pt3D(pt) =
	[pt[0], pt[1], 0];

function addPt(a, b) =
	[a[0] + b[0], a[1] + b[1], a[2] + b[2]];
	
function subPt(a, b) =
	[a[0] - b[0], a[1] - b[1], a[2] - b[2]];

function lerpPt(a, b, p) =
	[
		a[0] + (b[0] - a[0]) * p,
		a[1] + (b[1] - a[1]) * p,
		a[2] + (b[2] - a[2]) * p
	];

function length3D(pt) =
	sqrt(pt[0] * pt[0] + pt[1] * pt[1] + pt[2] * pt[2]);

// Preserve direction, change magnitude of 2D vector.
function normalize3D(pt, newLength = 1.0) =
	[
		pt[0] * (newLength / length3D(pt)), 
		pt[1] * (newLength / length3D(pt)),
		pt[2] * (newLength / length3D(pt))
	];
	
function rotateZ90(pt) =
	[pt[1], -pt[0], pt[2]];
	
function rotateZ(pt, angle) =
	[
		pt[0] * cos(angle) - pt[1] * sin(angle),
		pt[0] * sin(angle) + pt[1] * cos(angle),
		pt[2]
	];
	
// Difference between two adjacent points in an array
function ptArrayDelta2D(ar, index) =
	[
		ar[(index + 1) % len(ar)][0] - ar[index][0],
		ar[(index + 1) % len(ar)][1] - ar[index][1],
		0
	];

//
//  MOUNTAINS
//

module Mountain(idx) {
	translate(MOUNTAIN_POS[idx]) {		
		linear_extrude(height = MOUNTAIN_HEIGHTS[idx], scale = 0) {
			polygon(MOUNTAIN_POLYS[idx]);
		}
	}
}

module MountainCore(idx) {
	poly = MOUNTAIN_POLYS[idx];

	translate([0, 0, -HAIRLINE]) {

		intersection_for(i = [0 : len(poly) - 1]) {
			delta = ptArrayDelta2D(poly, i);
			move = normalize3D(rotateZ90(delta), WALL);

			translate([move[0], move[1], 0])
				Mountain(idx);
		}
	}
}

module AllMountains() {
	for (i = [0:MOUNTAIN_COUNT]) {
		Mountain(i);
	}
}

module AllMountainCores() {
	for (i = [0:MOUNTAIN_COUNT]) {
		MountainCore(i);
	}
}

//
//  BASE
//

module BasePoly() {
	for (i = [0:MOUNTAIN_COUNT]) {
		translate(MOUNTAIN_POS[i])
			polygon(MOUNTAIN_POLYS[i]);
	}
}

// 2D ring around the base, inset from the edge,
// with center cutout so max stroke is BASE_INSET.
module BaseRing2D(insetFromEdge = 0) {
	difference() {
		offset(r = -insetFromEdge)
			BasePoly();
		offset(r = -BASE_INSET)
			BasePoly();
	}
}

// Avoid weird corners at the base: Draw multiple layers,
// chamfer in from the outer edge.
module BaseFootprint() {
	LAYERS = 5;
	LAYER_H = BASE_HEIGHT / LAYERS;
	
	for (i = [0:LAYERS - 1]) {
		chamfer = (i + 1) * (WALL / LAYERS);
		
		translate([0, 0, i * LAYER_H])
			linear_extrude(height = LAYER_H + HAIRLINE)
				BaseRing2D(chamfer);
	}
}

module Floor() {
	linear_extrude(height = BASE_HEIGHT) {
		offset(r = -(BASE_INSET + 1.0))
			BasePoly();
	}
}

//
//  DISPLAY
//

module DisplayCutout() {
	Z = 20;	// cut depth
	
	// Pivot on bottom-center of screen,
	// cut into side of mountain.
	translate([-SCREEN_W / 2, 0, -Z / 2]) {
		
		minkowski() {
			translate([SCREEN_CORNER_R, SCREEN_CORNER_R, 0])
				cube([
					SCREEN_W - 2 * SCREEN_CORNER_R,
					SCREEN_H - 2 * SCREEN_CORNER_R,
					Z
				]);
			
			cylinder(h = HAIRLINE, r = SCREEN_CORNER_R);
		}
		
		// Screws
		for (i = [0:1]) {
			for (j = [0:1]) {
				translate([
					i ? (SCREEN_W - 0.5) : -7.5,
					j ? (SCREEN_H + 5.25) : -5.25,
					0
				])
					cylinder(h = Z, r = SCREW_R);
			}
		}
		
	}	// !pivot on bottom-center
}

module DisplayCutoutPosition() {
	poly = MOUNTAIN_POLYS[1];
	pos = MOUNTAIN_POS[1];
	height = MOUNTAIN_HEIGHTS[1];
	
	rightPt = addPt(pt3D(poly[0]), pos);
	leftPt = addPt(pt3D(poly[1]), pos);
	basePt = lerpPt(leftPt, rightPt, 0.5);

	diffPt = subPt(poly[0], poly[1]);
	tiltZ = atan2(diffPt[1], diffPt[0]);
	
	// Compute tiltX, so the cutout is parallel to the mountain face.
	apex = [
		pos[0] - basePt[0],
		pos[1] - basePt[1],
		height
	];
	apexAngle = atan2(apex[1], apex[0]);
	apexRot = rotateZ(apex, -apexAngle);	// Apex point is above X axis
	
	tiltX = atan2(apexRot[0], apexRot[2]);
	
	translate(basePt)
		rotate([0, 0, tiltZ])
			rotate([90 - tiltX, 0, 0])
				translate([SCREEN_X, SCREEN_Y, 0])
					DisplayCutout();
}

//
//  CORD
//

module CordCyl(fn) {
	poly = MOUNTAIN_POLYS[1];
	pos = MOUNTAIN_POS[1];
	PT_IDX = 4;
	
	rightPt = addPt(pt3D(poly[PT_IDX]), pos);
	leftPt = addPt(pt3D(poly[PT_IDX + 1]), pos);
	basePt = lerpPt(leftPt, rightPt, 0.5);
	
	diffPt = subPt(poly[PT_IDX], poly[PT_IDX + 1]);
	tiltZ = atan2(diffPt[1], diffPt[0]);
	
	translate([0, 0, CORD_Z])
		translate(basePt)
			rotate([0, 0, tiltZ])
				rotate([90, 0, 0])
					cylinder(h = 50, r = CORD_R, center = true, $fn = fn);
}

module CordCutout() {
	hull() {
		CordCyl(32);
		translate([0, 0, -100])
			CordCyl(4);
	}
}

//
//  PRINTER BED (Creality CR-10)
//

module PrinterFloor() {
	THICK = 10;
	rotate([0, 0, 45])
		translate([0, 0, -THICK / 2])
			cube([300, 300, THICK], center=true);
}

//
//  COMPOSITE
//

translate([0, 0, -100]) {	
	// TOP
	// Mountain heights, and display cutout
	difference() {
		union() {
			difference() {
				AllMountains();
				AllMountainCores();
			}
			BaseFootprint();
		}
			
		DisplayCutoutPosition();
		CordCutout();
	}
	
	// FLOOR PLATE
	Floor();
	
	//#PrinterFloor();
}

