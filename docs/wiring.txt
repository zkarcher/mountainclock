57 LEDs



ESP8266
-------
VCC
GND
3.3V -> to ePaper
23 -> DIN
18 -> CLK
5 -> CS
17 -> DC
16 -> RST
4 -> BUSY
15 == to SN74AHC pin 5
19 == OE, level shifter Output Enable (LEDs)



Level shifter: SN74AHC
----------------------
4 == GND
5 == Signal in (ESP pin 15)
6 == LED signal out
7 == GND
14 == +5V



ePaper
------
VCC (3.3V)         red
GND                black
DIN -> ESP 23      blue
CLK -> ESP 18      yellow
CS -> ESP 5        orange
DC -> ESP 17       green ?
RST -> ESP 16      white
BUSY -> ESP 4      grey
